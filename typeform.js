$.fn.upform = function () {
    var $count = 0;
    var $countWidth = 0;
    var $this = $(this);
    var container = $this.find(".upform-main");
    $(document).ready(function () {
        $(container).find(".input-block").first().click();
    });

    $($this).find("form").submit(function () {
        return false;
    });

    $(container)
        .find(".input-block,.input-block .bottom-btn .btn-ok")
        .not(".input-block input")
        .on("click", function () {
            if (($(this).attr('class')) == "btn btn-ok") {
                $count++;
                $countWidth = $countWidth + 0.83;
                $("#count").text($count);
                $(".bar").css("width", $countWidth + "vw");
                moveNext(this);
                rescroll(this)
            }
            if (($(this).attr('class')) == "input-block") {
                console.log('first click')
                rescroll(this)
            }

        });
    $(container)
        .find(".input-block .prev")
        .on("click", function () {
            $count--;
            $countWidth = $countWidth - 0.83;
            $("#count").text($count);
            $(".bar").css("width", $countWidth + "vw");
            movePrev(this)
        });
    $(container)
        .find(".input-block .next")
        .on("click", function () {
            $count++;
            $countWidth = $countWidth + 0.83;
            $("#count").text($count);
            if ($countWidth <= 20) {
                $(".bar").css("width", $countWidth + "vw");
            }
            moveNext(this)
        });
    $(container).find(".input-block input").keypress(function (e) {
        if (e.which == "") {
            $(this).parent().next().css("display", "none");
        } else {
            $(this).parent().next().css("display", "flex");
        }
        if (e.which == 13) {
            if ($(this).hasClass("required") && $(this).val() == "") { } else {
                $count++;
                $countWidth = $countWidth + 0.83;
                $("#count").text($count);
                $(".bar").css("width", $countWidth + "vw");
                moveNext(this);
            }
        }
    });
    $(container).find(".input-block textarea").keypress(function (e) {
        if (e.which == "") {
            $(this).parent().next().css("display", "none");
        } else {
            $(this).parent().next().css("display", "flex");
        }
        if (e.which == 13) {

        }
    });

    $(container).find(".input-block input").keydown(function (e) {
        if ($(container).find(".input-block input").val().length < 2) {
            $(this).parent().next().css("display", "none");
        } else {
            $(this).parent().next().css("display", "flex");
        }
    });

    $(container).find('.input-block input[type="radio"]').change(function (e) {
        $count++;
        $countWidth = $countWidth + 0.83;
        $("#count").text($count);
        $(".bar").css("width", $countWidth + "vw");
        moveNextRadio(this);


    });

    $(window).on("scroll", function () {
        $(container).find(".input-block").each(function () {
            var etop = $(this).offset().top;
            var diff = etop - $(window).scrollTop() + 20;

            if (diff > 100 && diff < 300) {
                reinitState(this);
            }

        });
    });

    function reinitState(e) {
        $(container).find(".input-block").removeClass("active");
        $(container).find(".input-block input").each(function () {
            $(this).blur();
        });
        $(e).addClass("active");
    }

    function rescroll(e) {
        if (($(e).attr('class').split(' ')[1]) != "btn-ok") {
            console.log('rescroll')
            $(window).scrollTo($(e), 400, {
                offset: { left: 200, top: -100 },
                queue: false,
                onAfter: function () {
                    $(e).addClass("active");
                    console.log('focus')
                    setTimeout(function () {
                        console.log('focus')
                        $(e).find(":input").focus();
                    }, 500);
                }
            });
        }
    }

    function reinit(e) {
        reinitState(e);
        rescroll(e);
    }

    function moveNext(e) {

        if ($count == 23) {
            console.log("airaxa")
            $(".next").css("display", "none");
            $(".done-btn").css("display", "inline");
        }
        $(e).parent().parent().next().click();
    }

    function moveNextRadio(e) {
        setTimeout(function () {
            console.log('movenext')
            $(e).parent().parent().next().click();
        }, 1000);

    }

    function movePrev(e) {
        $(e).parent().parent().prev().click();
    }
};

$(".upform").upform();
$('#btn-submit').click(function () {
    console.log('sending')
    $(".modal").css("display", "none");
    $(".upform").css("display", "none");
    $('.loader').css('display', 'block');
    var data = {
        service_id: 'gmail',
        template_id: 'template_c3wt9qk',
        user_id: 'user_qE7Sz0gP8o8kZ72o2Xl72',
        template_params: {
            from_name: "bibeklama",
            to_name: 'Sucheta',
            subject: "kaziquestions",
            message_html: "Testing"
        }
    };

    $.ajax('https://api.emailjs.com/api/v1.0/email/send', {
        type: 'POST',
        data: JSON.stringify(data),
        contentType: 'application/json'
    }).done(function () {
        $('.loader').css('display', 'none');
        alert('Your mail is sent!');
    }).fail(function (error) {
        alert('Oops…' + JSON.stringify(error));
    })
})